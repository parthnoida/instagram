import os
from flask import Flask, Blueprint
from config import Config


app = Flask(__name__, static_folder=os.path.join(os.path.dirname(__file__), "static"))
app.config.from_object(Config)


main = Blueprint('main', __name__)
#from views import *
from application import views
# from login import login
# from profile import profile
# from timeline import timeline

app.register_blueprint(main)
# app.register_blueprint(login)
# app.register_blueprint(profile)
# app.register_blueprint(timeline)


