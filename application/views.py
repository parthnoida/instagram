from . import main
from flask import current_app


@main.route('/')
def index():
    return current_app.make_response("Welcome to Instagram MVP")

